package com.spring_swagger.swagger_demo.dao;

import com.spring_swagger.swagger_demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface UserDaoImpl extends JpaRepository<User,Integer> {
}
