package com.spring_swagger.swagger_demo.service;

import com.spring_swagger.swagger_demo.dao.UserDaoImpl;
import com.spring_swagger.swagger_demo.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
public class UserService {

    @Autowired
    UserDaoImpl userDao;

    Logger log= LoggerFactory.getLogger(UserService.class);

    @Scheduled(fixedRate = 5000)
    public void add2DBJob(){
        User user=new User();
        user.setName("Salamon"+new Random().nextInt(374483));
        userDao.save(user);
        System.out.println("add service call in "+new Date().toString());
    }
    @Scheduled(cron = "0/15 * * * * *")
    public void fetchJob(){
        List<User> userList=userDao.findAll();
        System.out.println("fetch service call in "+new Date().toString());
        System.out.println("No of record found "+ userList.size());
        log.info("user {}",userList);
    }
}
